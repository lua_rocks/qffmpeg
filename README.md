## QFFMPEG

Wrapper for convenient use of the ffmpeg utility

## Installation

To install the latest release

```sh
git clone --depth 1 https://gitlab.com/lua_rocks/qffmpeg
cd qffmpeg
luarocks make
```

or

```sh
luarocks install https://gitlab.com/lua_rocks/qffmpeg/-/raw/master/qffmpeg.rockspec?ref_type=heads
```

With LuaRocks:
```sh
luarocks install qffmpeg
```

## Warnings

This is a alfa release. Changes can be made in the future.


## Copyright

See [LICENSE file](./LICENSE)

## TODO List

[+] extract audio from given files
[-] run ffmpeg in foreground
[-] track spent and remaining time
