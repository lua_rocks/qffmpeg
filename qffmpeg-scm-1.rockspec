---@diagnostic disable: lowercase-global
package = "qffmpeg"
version = "scm-1"

source = {
  url = "git+https://gitlab.com/lua_rocks/qffmpeg.git"
}

description = {
  summary = "wrapper for convenient use of the ffmpeg utility",
  detailed = [[
  ]],
  homepage = "https://gitlab.com/lua_rocks/qffmpeg",
  license = "MIT",
}

dependencies = {
  'lua >= 5.1',
  -- 'oop >= 0.5',
  'alogger >= 0.2.2',
  'cmd4lua >= 0.6.0',
  'cli-app-base >= 0.4.0',
}

build = {
  type = "builtin",
  modules = {
    ["qffmpeg-dev"] = "src/qffmpeg-dev.lua",
    ["qffmpeg.core"] = "src/qffmpeg/core.lua",
    ["qffmpeg.handler"] = "src/qffmpeg/handler.lua",
    ["qffmpeg.settings"] = "src/qffmpeg/settings.lua",
    ["qffmpeg.cmd.audio"] = "src/qffmpeg/cmd/audio.lua",
  }
}
