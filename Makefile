install:
	sudo luarocks make

APP := qffmpeg
VERSION := 0.2.0-1

upload:
	luarocks upload rockspecs/$(APP)-$(VERSION).rockspec

test:
	busted -p ".*%.lua$" test

link:
	ln -s ${PWD}/src/$(APP)-dev.lua ${HOME}/.local/bin/q$(APP)
