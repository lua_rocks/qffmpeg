#!/usr/bin/lua
-- 13-06-2024 @author Swarg
--

--
-- helper to get a real full path to the dir of the current script file
-- used to run app not from the project_root (i.g. via soft link from PATH)
--
local function get_app_dir()
  local file = io.popen('readlink -f ' .. tostring(arg[0]), 'r')
  if file then
    local output = file:read('*all')
    file:close()
    return output:match("(.*[/\\])") or output -- dir only w/o current filename
  end
  return 'src'
end

-- Global (stored in a _G.APP)
APP = { appdir = get_app_dir() }

package.path = package.path .. ";" .. APP.appdir .. '?.lua'

-- local builtin = require 'cli-app-base.builtin-cmds'
local settings = require 'qffmpeg.settings'
local handler = require 'qffmpeg.handler'

settings.setup()
handler.handle(arg)
