-- 13-06-2024 @author Swarg
--

local base = require 'cli-app-base'

local M = {}
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local filename_wo_ext = base.filename_without_extension

M.DEF_AUDIO_BITRATE = 64

---@param files table
---@param istart number?
---@param iend number?
---@return table
function M.sort_files_by_index(files, istart, iend)
  files = files
  local map = {}
  for _, fn in ipairs(files) do
    local idx = tonumber(string.match(fn, '^(%d+)'))
    if idx then
      map[idx] = fn
    else
      print('cannot sort list of files without indexes stop at file: ', fn)
      return files
    end
  end

  local t = {}
  local keys = base.tbl_sorted_keys(map)
  istart = istart or 1
  iend = iend or #keys
  for i = istart, iend do -- key in ipairs(keys) do
    local key = keys[i]
    t[#t + 1] = map[key]
  end

  return t
end

--
-- ffmpeg -i 05*.mp4 -vn -map a ogg/05-audio.mp3
-- todo bitrate, async, repo st, speed, avg remain time
-- todo run in foreground and output only progress,speed and remains time
--
---@param files table
---@param format string
---@param bitrate number
function M.extract_audio(files, format, bitrate, outdir, dry_run)
  format = format or 'mp3'
  bitrate = bitrate or M.DEF_AUDIO_BITRATE

  if outdir and outdir ~= '' then
    outdir = base.ensure_dirname(outdir)
    base.mkdir(outdir)
  else
    outdir = ''
  end

  local sz = #files
  for i, fn in ipairs(files) do
    local ofn = outdir .. filename_wo_ext(fn) .. '.' .. format
    local cmd = fmt('ffmpeg -i "%s" -vn -map a -b:a %sk "%s"', fn, bitrate, ofn)
    print(fmt('%3s/%3s', i, sz), cmd)
    -- todo output to stdout
    local ret = base.exec(cmd, dry_run)
    print('exitcode ok:', ret)
  end
end

--
return M
