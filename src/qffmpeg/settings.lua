-- 13-06-2024 @author Swarg


local log = require 'alogger'

local base = require 'cli-app-base'
local builtin = require 'cli-app-base.builtin-cmds'

local M = {}

M.appname = 'qffmpeg'
M.config_file = nil
M.config = nil

local HOME = os.getenv('HOME')
local HOT_DEBUG = os.getenv('QFFMPEG_DEBUG')

M.CONF_DIR = HOME .. '/.config/' .. M.appname .. '/'
M.SHARE_DIR = HOME .. '/.local/share/' .. M.appname .. '/'

local default_config = {
  editor = 'nvim',
  logger = {
    save = true,
    level = log.levels.INFO,
    appname = M.appname,
  }
}

---@param conf table
local function apply_env_vars(conf)
  conf = conf
  if HOT_DEBUG then
    log.fast_setup()
  end
end

function M.setup()
  M.config, M.config_file = base.setup(
    default_config, M.CONF_DIR, M.SHARE_DIR, apply_env_vars, HOT_DEBUG ~= nil
  )
  -- bind self to buildint commands such as logger, config
  builtin.settings = M
end

--
-- save config file
--
---@param verbose boolean?
---@return boolean
function M.save(verbose)
  local f = base.save_config(M.config_file, M.config)
  local msg = M.config_file .. ' saved: ' .. tostring(f)
  if verbose then
    print(msg)
  end
  return f
end

-- raise error
---@return boolean
function M.validate_config()
  if type(M.config) ~= 'table' then
    error('no settings.config')
  end
  if type(M.config.projects) ~= 'table' then
    error('no settings.config.projects')
  end
  return true
end

return M
