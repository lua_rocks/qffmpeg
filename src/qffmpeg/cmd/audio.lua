-- 13-06-2024 @author Swarg
--
-- Goal:
--   - extract audio files
--

local log = require 'alogger'
local core = require 'qffmpeg.core'

local M = {}

--
--------------------------------------------------------------------------------
--                         COMMANDS INTERFACE
--------------------------------------------------------------------------------

---@param w Cmd4Lua
function M.handle(w)
  w:about('audio')
      :handlers(M)

      :desc('extract audio track from given file or path')
      :cmd('extract', 'e')

      :run()
end

--
--------------------------------------------------------------------------------
--                      COMMANDS  IMPLEMENTATIONS
--------------------------------------------------------------------------------


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
-- local join_path = base.join_path
-- local get_parent_dir = base.get_parent_dirpath


--
-- extract audio track from given file or path
--
---@param w Cmd4Lua
function M.cmd_extract(w)
  w:v_opt_dry_run('-d')
  local files = w:desc('list of files with media-files'):rest_args()
  local format = w:desc('audio format'):def('mp3'):opt('--format', '-f')
  local bitrate = w:desc('output directory')
      :def(core.DEF_AUDIO_BITRATE):optn('--bitrate', '-b')
  local odir = w:desc('output directory'):opt('--output-dir', '-D')
  -- simple filter
  local istart = w:desc('end index in files lit to filter from given list')
      :optn('--start', '-s')
  local iend = w:desc('end index in files lit to filter from given list')
      :optn('--end', '-e')

  if not w:is_input_valid() then return end
  files = core.sort_files_by_index(files, istart, iend)
  local ok, ret = core.extract_audio(files, format, bitrate, odir, w:is_dry_run())
  if not ok then return w:error(ok) end

  w:say(ret)
end

return M
