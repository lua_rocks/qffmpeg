-- 13-06-2024 @author Swarg
--

local Cmd4Lua = require 'cmd4lua'
local builtin = require 'cli-app-base.builtin-cmds'
local audio = require 'qffmpeg.cmd.audio'


local M = {}
M.VERSION = 'qffmpeg v0.2.0'
M._URL = 'https://gitlab.com/lua_rocks/qffmpeg'

---@param args string|table
function M.handle(args)
  Cmd4Lua.of(args)
      :root('qffmpeg')
      :about('cli wrapper around ffmpeg')

      :desc('Built-in commands of the appliction, such as Config, Logger, Reload')
      :cmd('builtin', 'b', builtin.handle)

      :desc('extract audio from given files')
      :cmd('audio', 'a', audio.handle)

      :run()
      :exitcode()
end

return M
