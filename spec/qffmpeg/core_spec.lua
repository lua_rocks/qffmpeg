-- 13-06-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'qffmpeg.core'

describe("qffmpeg.core", function()
  it("sort_files_by_index", function()
    local list = { '11-fn.mp4', '12-fn.mp4', '1-fn.mp4', '2-file name.mp4' }
    local exp = { '1-fn.mp4', '2-file name.mp4', '11-fn.mp4', '12-fn.mp4' }
    assert.same(exp, M.sort_files_by_index(list))
  end)

  it("sort_files_by_index space", function()
    local list = { '11-fn.mp4', '12 fn.mp4', '1-fn.mp4', '2-file name.mp4' }
    local exp = { '1-fn.mp4', '2-file name.mp4', '11-fn.mp4', '12 fn.mp4' }
    assert.same(exp, M.sort_files_by_index(list))
  end)

  it("sort_files_by_index no index", function()
    local list = { '11-fn.mp4', 'BAD 12 fn.mp4', '1-fn.mp4', '2-file name.mp4' }
    local exp = { '11-fn.mp4', 'BAD 12 fn.mp4', '1-fn.mp4', '2-file name.mp4' }
    assert.same(exp, M.sort_files_by_index(list))
  end)
end)
